<?php

namespace LendFlow\Core\Providers;

class CoreServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();
        }

        $this->registerConfigs();

        $this->registerTranslations();
    }

    /**
     * Register Core's translation files.
     *
     * @return void
     */
    protected function registerTranslations(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../../assets/lang', 'core');

        $this->publishes([
            __DIR__.'/../../assets/lang' => resource_path('lang/vendor/lendflow/core'),
        ], 'lendflow::core-translations');
    }

    /**
     * Register Core's migration files.
     *
     * @return void
     */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../../assets/database/migrations');

        $this->publishes([
            __DIR__.'/../../assets/database/migrations' => database_path('migrations'),
        ], 'lendflow::core-migrations');
    }

    /**
     * Register Core's config files.
     *
     * @return void
     */
    protected function registerConfigs(): void
    {
        $path = realpath(__DIR__.'/../../assets/config/config.php');

        $this->mergeConfigFrom($path, 'lendflow');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }
}
